import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { RegistrationComponent } from './registration/registration.component';
import { HomeComponent } from './home/home.component';
import { SigninComponent } from './signin/signin.component';


import { FormsModule } from '@angular/forms'; 
import { HttpClientModule } from '@angular/common/http'; 
// https://www.npmjs.com/package/@ng-bootstrap/ng-bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'; 

import { RestApiService } from './rest-api.service';
import { DataService } from './data.service';
import { AuthService } from './auth.service';
import { AboutComponent } from './about/about.component';



@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    HomeComponent,
    SigninComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, // ??? .forRoot()  not this one??
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot()
  ],
  providers: [RestApiService, DataService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }

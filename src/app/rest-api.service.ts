import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class RestApiService {

  constructor(private http: HttpClient) { }

  // ??? dont get it
  getHeaders() {
    const token = localStorage.getItem('token');
    return token ? new HttpHeaders().set('Authorization', token) : null;
  } 

  get(link: string) {
    // promise (become asynchonized function)
    return this.http.get(link, { headers: this.getHeaders() }).toPromise();
  }

  post(link: string, body: any) {
    // promise (become asynchonized function)
    return this.http.post(link, body, { headers: this.getHeaders() }).toPromise();
  }
}

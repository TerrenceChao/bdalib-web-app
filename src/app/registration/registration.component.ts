import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from '../data.service';
import { RestApiService } from '../rest-api.service';



@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {

  name = '';
  email = '';
  password = '';
  password1 = '';

  btnDisabled = false;

  constructor(
    private router: Router,
    private data: DataService,
    private rest: RestApiService
  ) { }
 
  ngOnInit() {
  }

  validate() {
    if ( ! this.name) {
      this.data.error('Name is ont entered');
      return; 
    }

    if ( ! this.email) {
      this.data.error('Email is not entered');
      return;
    }

    if ( ! this.password) {
      this.data.error('Password is not entered');
      return;
    }

    if ( ! this.password1) {
      this.data.error('Confirmation Password is not entered');
      return;
    }

    if (this.password === this.password1) {
      return true;
    } else {
      this.data.error('Passwords do not match.');
      return;
    }

  }

  async register() {
    this.btnDisabled = true;
    try {
      if (this.validate()) {
        const data = await this.rest.post(
          'http://localhost:3030/api/account/signup',
          {
            name: this.name,
            email: this.email,
            password: this.password
          }
        );

        if (data['success']) {
          localStorage.setItem('token', data['token']);
          this.data.success('Registration successful !');
        } else {
          this.data.error(data['message']);
        }
      }
    } catch(error) {
      this.data.error(error['message']);
    }
    this.btnDisabled = false;
  }

}
